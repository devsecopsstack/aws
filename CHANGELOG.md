## [5.2.3](https://gitlab.com/to-be-continuous/aws/compare/5.2.2...5.2.3) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([f13b8de](https://gitlab.com/to-be-continuous/aws/commit/f13b8de4967cd24997f3c7a41f28f199c52f306e))

## [5.2.2](https://gitlab.com/to-be-continuous/aws/compare/5.2.1...5.2.2) (2024-04-06)


### Bug Fixes

* avoid printing OIDC credentials in stdout ([0c27431](https://gitlab.com/to-be-continuous/aws/commit/0c27431ca85af340c96442111e58b8630613fedd))

## [5.2.1](https://gitlab.com/to-be-continuous/aws/compare/5.2.0...5.2.1) (2024-04-03)


### Bug Fixes

* **vault:** use vault-secrets-provider's "latest" image tag ([9f4bb37](https://gitlab.com/to-be-continuous/aws/commit/9f4bb37d2bd31759a18b82a00fca122d7bc6cdba))

# [5.2.0](https://gitlab.com/to-be-continuous/aws/compare/5.1.0...5.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([5a52095](https://gitlab.com/to-be-continuous/aws/commit/5a52095066a790558ce95cf6be12b2f7e9402666))

# [5.1.0](https://gitlab.com/to-be-continuous/aws/compare/5.0.2...5.1.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([226acf8](https://gitlab.com/to-be-continuous/aws/commit/226acf89e54c0676b7ecc8e51ab6150592270107))

## [5.0.2](https://gitlab.com/to-be-continuous/aws/compare/5.0.1...5.0.2) (2023-12-2)


### Bug Fixes

* envsubst when variable contains a '&' ([2d9d331](https://gitlab.com/to-be-continuous/aws/commit/2d9d331e4a93f5cf42f7e577aa9529b8f21a207a))

## [5.0.1](https://gitlab.com/to-be-continuous/aws/compare/5.0.0...5.0.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([e972f59](https://gitlab.com/to-be-continuous/aws/commit/e972f594424390749909c906c422ab2b8c81695b))

# [5.0.0](https://gitlab.com/to-be-continuous/aws/compare/4.1.0...5.0.0) (2023-09-26)


* feat!: support environment auto-stop ([e26561b](https://gitlab.com/to-be-continuous/aws/commit/e26561b7c2074de1f2fbc41fdc7246047592ceab))


### BREAKING CHANGES

* now review environments will auto stop after 4 hours
by default. Configurable (see doc).

# [4.1.0](https://gitlab.com/to-be-continuous/aws/compare/4.0.0...4.1.0) (2023-09-02)


### Features

* allow propagate custom output variables ([413eb25](https://gitlab.com/to-be-continuous/aws/commit/413eb25c7fa32fcebf966ea654e791b6732591a8))

# [4.0.0](https://gitlab.com/to-be-continuous/aws/compare/3.2.0...4.0.0) (2023-08-28)


### Features

* **oidc:** OIDC authentication support now requires  explicit configuration (see doc) ([451b02f](https://gitlab.com/to-be-continuous/aws/commit/451b02f6f5a721c7429700795f318502cda9682e))
* support ID tokens instead of CI_JOB_JWT ([d825692](https://gitlab.com/to-be-continuous/aws/commit/d825692ee961a12c58dfb16b07ffba2524ffd9b7))


### BREAKING CHANGES

* **oidc:** OIDC authentication support now requires  explicit configuration (see doc)

# [3.2.0](https://gitlab.com/to-be-continuous/aws/compare/3.1.0...3.2.0) (2023-08-07)


### Features

* support ID tokens in addition to CI_JOB_JWT ([1d91462](https://gitlab.com/to-be-continuous/aws/commit/1d914626ef3599112319608ff50702bedb7a006e))

# [3.1.0](https://gitlab.com/to-be-continuous/aws/compare/3.0.0...3.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([a2be867](https://gitlab.com/to-be-continuous/aws/commit/a2be8677186b28cf14e04e7e665b359672d83c3d))

# [3.0.0](https://gitlab.com/to-be-continuous/aws/compare/2.3.2...3.0.0) (2023-04-05)


### Features

* **deploy:** redesign deployment strategy ([2b95346](https://gitlab.com/to-be-continuous/aws/commit/2b95346d7b25cdf9bc368297c827c536d2ca3d5b))


### BREAKING CHANGES

* **deploy:** $AUTODEPLOY_TO_PROD no longer supported (replaced by $AWS_PROD_DEPLOY_STRATEGY - see doc)

## [2.3.2](https://gitlab.com/to-be-continuous/aws/compare/2.3.1...2.3.2) (2023-01-27)


### Bug Fixes

* Add registry name in all Docker images ([4b7dfe2](https://gitlab.com/to-be-continuous/aws/commit/4b7dfe24265255e8af106d9b725f39df510475b9))

## [2.3.1](https://gitlab.com/to-be-continuous/aws/compare/2.3.0...2.3.1) (2022-12-17)


### Bug Fixes

* hanging awk script ([bac3bd1](https://gitlab.com/to-be-continuous/aws/commit/bac3bd175ade8921739ab0b3e838fd7a4524c77d))

# [2.3.0](https://gitlab.com/to-be-continuous/aws/compare/2.2.0...2.3.0) (2022-12-15)


### Features

* improve environments url ([ea4f606](https://gitlab.com/to-be-continuous/aws/commit/ea4f606aec9e03b5874501a74c79f512b784e4e3))

# [2.2.0](https://gitlab.com/to-be-continuous/aws/compare/2.1.0...2.2.0) (2022-12-13)


### Features

* **vault:** configurable Vault Secrets Provider image ([b274cda](https://gitlab.com/to-be-continuous/aws/commit/b274cdaf8bce2bbfc7bc3814a6446f0aa0c68df3))

# [2.1.0](https://gitlab.com/to-be-continuous/aws/compare/2.0.0...2.1.0) (2022-10-11)


### Features

* authenticate with OpenID Connect ([fb900e7](https://gitlab.com/to-be-continuous/aws/commit/fb900e7f180d9fcc40e0b6cca2741e4b202a1ce5))

# [2.0.0](https://gitlab.com/to-be-continuous/aws/compare/1.4.0...2.0.0) (2022-08-05)


### Bug Fixes

* trigger new major release ([5d0493c](https://gitlab.com/to-be-continuous/aws/commit/5d0493cb27788725a6d5b381e7ae4d24b54d415d))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [1.4.0](https://gitlab.com/to-be-continuous/aws/compare/1.3.0...1.4.0) (2022-08-05)


### Features

* make MR pipeline the default workflow ([54ffaac](https://gitlab.com/to-be-continuous/aws/commit/54ffaace4e9d73d330a4952c17bd98e0ac5bb43f))

# [1.3.0](https://gitlab.com/to-be-continuous/aws/compare/1.2.0...1.3.0) (2022-06-30)


### Features

* enforce AUTODEPLOY_TO_PROD and PUBLISH_ON_PROD as boolean variables ([daf2a23](https://gitlab.com/to-be-continuous/aws/commit/daf2a233a17e8415bb5d66b13ada4403bc0a5233))

# [1.2.0](https://gitlab.com/to-be-continuous/aws/compare/1.1.3...1.2.0) (2022-05-01)


### Features

* configurable tracking image ([3642018](https://gitlab.com/to-be-continuous/aws/commit/364201802b03ed47fb5cd35e13793c2f79486a6f))

## [1.1.3](https://gitlab.com/to-be-continuous/aws/compare/1.1.2...1.1.3) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([754138e](https://gitlab.com/to-be-continuous/aws/commit/754138e631fb01875ad6ea8c66930438b7ca434f))

## [1.1.2](https://gitlab.com/to-be-continuous/aws/compare/1.1.1...1.1.2) (2022-02-23)


### Bug Fixes

* **vault:** Vault JWT authentication not working ([1d4704e](https://gitlab.com/to-be-continuous/aws/commit/1d4704e5afe121b9244f189050a4177e5d73fafa))

## [1.1.1](https://gitlab.com/to-be-continuous/aws/compare/1.1.0...1.1.1) (2022-01-30)


### Bug Fixes

* chmod issue on hook scripts ([cb33002](https://gitlab.com/to-be-continuous/aws/commit/cb330024e04dd4c13275688b005baf95a03f13fb))

# [1.1.0](https://gitlab.com/to-be-continuous/aws/compare/1.0.2...1.1.0) (2022-01-10)


### Features

* Vault variant + non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([bb9c0d1](https://gitlab.com/to-be-continuous/aws/commit/bb9c0d106ffc8af4db1882a2bd0663e2fb3296a5))

## [1.0.2](https://gitlab.com/to-be-continuous/aws/compare/1.0.1...1.0.2) (2021-12-03)


### Bug Fixes

* execute hook scripts with shebang shell ([e40e7d2](https://gitlab.com/to-be-continuous/aws/commit/e40e7d269b0f0ff905a67b83972e80ffd605363f))

## [1.0.1](https://gitlab.com/to-be-continuous/aws/compare/1.0.0...1.0.1) (2021-10-17)


### Bug Fixes

* support export functions to fail ([3d8d411](https://gitlab.com/to-be-continuous/aws/commit/3d8d4110bcac354fb387a798805431209aab084d))

# 1.0.0 (2021-10-09)


### Bug Fixes

* cleanup upstream deployment artifacts ([9bb5850](https://gitlab.com/to-be-continuous/aws/commit/9bb5850c31a54c8d844437a5752f26c451f9fa04))
* dynamic env url ([0735f0b](https://gitlab.com/to-be-continuous/aws/commit/0735f0b859bdaae891a73552a43a3131b02517a8))


### Features

* initial version ([7f8e456](https://gitlab.com/to-be-continuous/aws/commit/7f8e4563de6c6aaf6179a2399b4067a52448ac6e))

# 1.0.0 (2021-10-08)


### Bug Fixes

* dynamic env url ([0735f0b](https://gitlab.com/to-be-continuous/aws/commit/0735f0b859bdaae891a73552a43a3131b02517a8))


### Features

* initial version ([7f8e456](https://gitlab.com/to-be-continuous/aws/commit/7f8e4563de6c6aaf6179a2399b4067a52448ac6e))

# 1.0.0 (2021-10-08)


### Features

* initial version ([7f8e456](https://gitlab.com/to-be-continuous/aws/commit/7f8e4563de6c6aaf6179a2399b4067a52448ac6e))
